<?php

namespace console\controllers;

use Yii;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\Console;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use vgr\OpenFactura\ApiClient;


class OpenfacturaController extends Controller
{

    private static $apiKey = "Api-key-here";     
    

    public function actionBuscarRut($rut)
    {
        try {
            $client = new ApiClient(self::$apiKey);
            if($client->validateKey())
            {
                $client->taxpayer($rut);
            }
        }
        catch (Exception $e) {
            Console::output('Error while using the API.');
            return ExitCode::TEMPFAIL;
        }       
    }

    public function actionBuscarVentasDiarias(int $año, int $mes, int $dia)
    {
        try {
            $client = new ApiClient(self::$apiKey);
            if($client->validateKey())
            {
                $day = str_pad($dia, 2, '0', STR_PAD_LEFT); 
                $month = str_pad($mes, 2, '0', STR_PAD_LEFT);  
                $fecha = $año.'/'.$month.'/'.$day;                         
                if(strlen($fecha) === 10)
                {
                    $client->getSales($fecha);                
                }
                else
                {
                    Console::output('Fecha no valida, Formato YYYY/MM/DD Passed:'.$fecha);
                }
            }            
        }
        catch (Exception $e) {
            Console::output('Error while using the API.');
            return ExitCode::TEMPFAIL;
        }       
    }

    public function actionBuscarVentasMensuales(int $año, int $mes)
    {
        try {
            $client = new ApiClient(self::$apiKey);
            if($client->validateKey())
            {
                $month = str_pad($mes, 2, '0', STR_PAD_LEFT);               
                $fecha = $año.'/'.$month;                        
                if(strlen($fecha) === 7)
                {
                    $client->getSales($fecha);                
                }
                else
                {
                    Console::output('Fecha no valida, Formato YYYY/MM Passed:'.$fecha);
                }
            }            
        }
        catch (Exception $e) {
            Console::output('Error while using the API.');
            return ExitCode::TEMPFAIL;
        }       
    }

    public function actionBuscarComprasDiarias(int $año, int $mes, int $dia, $estado)
    {
        try {
            $client = new ApiClient(self::$apiKey);
            if($client->validateKey())
            {
                $status;
                $day = str_pad($dia, 2, '0', STR_PAD_LEFT); 
                $month = str_pad($mes, 2, '0', STR_PAD_LEFT);  
                $fecha = $año.'/'.$month.'/'.$day;                         
                if(strlen($fecha) === 10)
                {
                    switch (true) {
                    case ($estado == 'Pendiente'):
                      $status = 'pending';
                    break;
                    case ($estado == 'Registrado'):
                      $status = 'registered';
                    break;
                    case ($estado == 'NoIncluir'):
                      $status = 'exclude';
                    break;
                    case ($estado == 'Reclamado'):
                      $status = 'reclaimed';
                    break;
                    default:
                      $status = null;
                    }              
                    if($estado !== null)
                    {
                        $client->getPurchases($fecha,$estado);
                    }
                    else
                    {
                        $client->getPurchases($fecha);
                    }                             
                }
                else
                {
                    Console::output('Fecha no valida, Formato YYYY/MM/DD');
                }
            }  
                   
        }
        catch (Exception $e) {
            Console::output('Error while using the API.');
            return ExitCode::TEMPFAIL;
        }       
    }

    public function actionBuscarComprasMensuales(int $año, int $mes, $estado)
    {
        try {
            $client = new ApiClient(self::$apiKey);
            if($client->validateKey())
            { 
                $status;          
                $month = str_pad($mes, 2, '0', STR_PAD_LEFT);               
                $fecha = $año.'/'.$month;                        
                if(strlen($fecha) === 7)
                {            
                    switch (true) {
                    case ($estado == 'Pendiente'):
                      $status = 'pending';
                    break;
                    case ($estado == 'Registrado'):
                      $status = 'registered';
                    break;
                    case ($estado == 'NoIncluir'):
                      $status = 'exclude';
                    break;
                    case ($estado == 'Reclamado'):
                      $status = 'reclaimed';
                    break;
                    default:
                      $status = null;
                    }                   
                    if($status !== null)
                    {
                        $client->getPurchases($fecha,$status);
                    }
                    else
                    {
                        $client->getPurchases($fecha); 
                    }                                                                               
                }                   
                else
                {
                    Console::output('Fecha no valida, Formato YYYY/MM');
                }
            }             
        }
        catch (Exception $e) {
            Console::output('Error while using the API.');
            return ExitCode::TEMPFAIL;
        }       
    }

    public function actionBuscarToken($token, $formato)
    {
        try {
            $client = new ApiClient(self::$apiKey);            
            if($client->validateKey())
            {
                if($formato = $this->validFormat)
                {
                    $client->searchByToken($token, $formato);
                }
                else
                {
                     Console::output('Formato no soportado por OpenFactura');
                }
            }            
        }
        catch (Exception $e) {
            Console::output('Error while using the API.');
            return ExitCode::TEMPFAIL;
        }       
    }

    public function actionBuscarDocumento($rut, int $dte, int $numeroDoc, $formato)
    {
        try {            
            $client = new ApiClient(self::$apiKey);          
            if($client->validateKey())
            {
                $format;           
                switch (true) {
                    case ($formato == 'Estado'):
                      $format = 'status';
                    break;
                    case ($formato == 'Xml'):
                      $format = 'xml';
                    break;
                    case ($formato == 'Json'):
                      $format = 'json';
                    break;                   
                    default:
                      $format = null;
                    }
                if($format !== null)
                {                
                    $client->searchDocument($rut,$dte,$numeroDoc,$format);
                }
                else
                {
                     Console::output('Formato no soportado por OpenFactura');
                }
            }            
        }
        catch (Exception $e) {
            Console::output('Error while using the API.');
            return ExitCode::TEMPFAIL;
        }       
    }
}
