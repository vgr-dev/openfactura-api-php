# openfactura-api-php

Unofficial wrapper for OpenFactura API

Developed on Yii2 Framework.

By VGR-SPA

[Visita Nuestro Sitio](https://www.vgr.cl)

## Install
Our Package can be installed with Composer:
1. On your composer.json on the "Require" section add "vgr-dev/openfactura-api-php": "1.0". 
Or
2. Run on your proyect composer require command, search for "vgr-dev" make sure you install the last Stable Version.  

## Features

```bash
    
     Version 1.0
    ==========================================================================================  

	Novedades: 
		- Consultas de tipo GET a Openfactura 100% funcionales.
		- Facil manejo mediante consola.

    ==========================================================================================
   
  
