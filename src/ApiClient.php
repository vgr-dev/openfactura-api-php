<?php

namespace vgr\OpenFactura;

use yii\helpers\Console;

class ApiClient
{  
    const USER_AGENT_SUFFIX = 'openfactura-api-php';    
    const API_BASE_PATH = 'https://dev-api.haulmer.com/v2/dte/';
    const HttpVersion = 'CURL_HTTP_VERSION_1_1'; 
    const CurlTimeOut = 20; 
    const EVENT_STATE_ALL = 'ALL';
    const EVENT_STATE_ACTIVE = 'ACTIVE';
    const EVENT_STATE_CLOSED = 'CLOSED';            

    /**
     * Returns all the possible event states.
     *
     * @return array
     */
    public static function eventStates(): array
    {
        return [
            self::EVENT_STATE_ALL,
            self::EVENT_STATE_ACTIVE,
            self::EVENT_STATE_CLOSED,
        ];
    } 

    protected $apiKey;

    private $validationStatusResponse = 'Failed';

    private $connectionAuthorized = false;    

    private $url;

    private $header;

    private $baseRequest = array(           
           CURLOPT_RETURNTRANSFER => true,        
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => self::CurlTimeOut,
           CURLOPT_HTTP_VERSION => self::HttpVersion
          );

    public function __construct(string $key)
    {
        $this->apiKey = $key;
        $this->header = array("apikey: ". $this->apiKey);    
    }   

    public function get($path)
    {
        return $this->execute($path,'GET');
    }         

    public function execute($path, $method, $data = null)
    {
        if(!empty($path))
        {
            $this->url = self::API_BASE_PATH.$path;            
        }
        else
        {
            throw new Exception("Request not valid url", $path);
        }
          $curl = curl_init($this->url);          
          curl_setopt($curl, CURLOPT_HTTPHEADER, $this->header);
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);         
          curl_setopt_array($curl, $this->baseRequest);

        if ($data !== null)
        {
          curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        }

          $response = curl_exec($curl);    
          $error = curl_error($curl);

          curl_close($curl);

          if (!$error)
          {
            Console::output($response);                     
          } 
          else
          {
            Console::output("Error Processing Request");               
          }               
    }

    public function validateKey(): string
    {
        if(!empty($this->apiKey))
        {
          if(strlen($this->apiKey) === 32)
          {
            $this->validationStatusResponse = 'Authorized';
            $this->connectionAuthorized = true;
          }
          else
          {
            Console::output("Your ApiKey will not match with OpenFactura apiKey format");               
          }                     
        }
        else
        {
           Console::output("You must set the ApiKey to access the commands"); 
        }

        $result = 'Credentials Validation has run, Status has change to: '.$this->validationStatusResponse;
        Console::output($result);        
        return $this->connectionAuthorized;                            
    }

    public function taxpayer(string $rut)
    {
        $path = 'taxpayer/'.$rut;                      
        return $this->get($path);
    }

    public function getSales(string $date)
    {
        $path = "registry/sales/".$date;
         return $this->get($path);
    }
    
    public function getPurchases(string $date, $status = null)
    {
        if($status !== null)
        {
          $path = "registry/purchase/".$date."?status=".$status;
        }
        else
        {
          $path = "registry/purchase/".$date;
        }
         return $this->get($path);
    }

    public function searchByToken(string $token, string $value)
    {
        $path = $token."/".$value;
        return $this->get($path);
    }

    public function searchDocument(string $rut, string $type, int $docNumber, string $value)
    {
        $path = $rut."/".$type."/".$docNumber."/".$value;
        return $this->get($path);
    }
}  