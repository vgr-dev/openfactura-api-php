<?php

use vgr\OpenFactura\ApiClient;

class ValidateTest extends \Codeception\Test\Unit
{

    use \Codeception\Specify;    

    public function testValidateKey()
    {        
        $this->specify("You must set the ApiKey", function(){
            $client = new ApiClient('default');            
            $this->assertInternalType('string', $client->validateKey());          
        });       
    }
}